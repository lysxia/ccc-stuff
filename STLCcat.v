From Coq Require Import List.
From Coq Require Setoid.
From Coq Require Import Equivalence Morphisms.

Import ListNotations.

Inductive ty : Set :=
| Arr : ty -> ty -> ty
| T : ty
| Prod : ty -> ty -> ty
.

Delimit Scope ty_scope with ty.

Infix "->" := Arr : ty_scope.

Fixpoint var (g : list ty) (t : ty) : Set :=
  match g with
  | nil => Empty_set
  | cons t' g' => eq t' t + var g' t
  end.

Definition here_ {g : list ty} {t t' : ty} : t = t' -> var (t :: g) t' :=
  fun e => inl e.

Definition here {g : list ty} {t : ty} : var (t :: g) t :=
  inl eq_refl.

Definition there {g : list ty} {t t1 : ty} : var g t -> var (t1 :: g) t :=
  inr.

Inductive tm (g : list ty) : ty -> Set :=
| Var : forall t, var g t -> tm g t
| App : forall t1 t2, tm g (Arr t1 t2) -> tm g t1 -> tm g t2
| Lam : forall t1 t2, tm (t1 :: g) t2 -> tm g (Arr t1 t2)
| Pair : forall t1 t2, tm g t1 -> tm g t2 -> tm g (Prod t1 t2)
| Proj1 : forall t1 t2, tm g (Prod t1 t2) -> tm g t1
| Proj2 : forall t1 t2, tm g (Prod t1 t2) -> tm g t2
.

Arguments Var {g t}.
Arguments App {g t1 t2}.
Arguments Lam {g t1 t2}.
Arguments Pair {g t1 t2}.
Arguments Proj1 {g t1 t2}.
Arguments Proj2 {g t1 t2}.

Notation tm0 := (tm nil).

Notation "A ---> B" := (tm0 (Arr A B)) (at level 80).

Fixpoint shift_var {g1 g2 t t0} : var (g1 ++ g2) t -> var (g1 ++ t0 :: g2) t :=
  match g1 with
  | nil => fun v => inr v
  | cons _ g1' => fun v =>
    match v with
    | inl e => inl e
    | inr v' => inr (shift_var v')
    end
  end.

Fixpoint shift {g1 g2 t t0} (u : tm (g1 ++ g2) t) : tm (g1 ++ t0 :: g2) t :=
  match u with
  | Var v => Var (shift_var v)
  | App u1 u2 => App (shift u1) (shift u2)
  | Lam u' => Lam (shift (g1 := _ :: g1) u')
  | Pair u1 u2 => Pair (shift u1) (shift u2)
  | Proj1 u' => Proj1 (shift u')
  | Proj2 u' => Proj2 (shift u')
  end.

Notation shift0 := (shift (g1 := nil)).

Fixpoint subst_var_ {g1 g2 t t1} {R : Set}
         (h1 : (t1 = t) -> R)
  : (var (g1 ++ g2) t -> R) ->
    var (g1 ++ t1 :: g2) t -> R :=
  match g1 with
  | nil => fun h2 v =>
    match v with
    | inl e => h1 e
    | inr v' => h2 v'
    end
  | cons t0 g1' => fun h2 v =>
    match v with
    | inl e => h2 (here_ e)
    | inr v' => subst_var_ h1 (fun w => h2 (inr w)) v'
    end
  end.

Definition subst_var {g1 g2 t t1}
  (v : var (g1 ++ t1 :: g2) t) (u : tm (g1 ++ g2) t1) : tm (g1 ++ g2) t :=
  subst_var_ (eq_rec t1 (tm (g1 ++ g2)) u t) Var v.

Fixpoint subst {g1 g2 t t0} (u1 : tm (g1 ++ t0 :: g2) t) (u0 : tm (g1 ++ g2) t0) : tm (g1 ++ g2) t :=
  match u1 with
  | Var v => subst_var v u0
  | App u1 u2 => App (subst u1 u0) (subst u2 u0)
  | Lam u => Lam (subst (g1 := _ :: g1) u (shift0 u0))
  | Pair u1 u2 => Pair (subst u1 u0) (subst u2 u0)
  | Proj1 u => Proj1 (subst u u0)
  | Proj2 u => Proj2 (subst u u0)
  end.

Definition subst0 {g t t1} : tm (t1 :: g) t -> tm g t1 -> tm g t :=
  subst (g1 := nil).

Definition eq_tm {g t} : tm g t -> tm g t -> Prop.
Admitted.

Delimit Scope tm_scope with tm.

Infix "=" := eq_tm : tm_scope.

Local Open Scope tm_scope.

Instance Equivalence_eq_tm {g t} : Equivalence (@eq_tm g t).
Proof.
Admitted.

Lemma red_beta {g t t1} (u1 : tm (t1 :: g) t) u2
  : App (Lam u1) u2 = subst0 u1 u2.
Admitted.

Lemma red_proj1 {g t1 t2} (u1 : tm g t1) (u2 : tm g t2)
  : Proj1 (Pair u1 u2) = u1.
Admitted.

Lemma red_proj2 {g t1 t2} (u1 : tm g t1) (u2 : tm g t2)
  : Proj2 (Pair u1 u2) = u2.
Admitted.

Lemma subst_shift_cancel {g1 g2 t t0} (u1 : tm (g1 ++ g2) t) (u2 : tm (g1 ++ g2) t0)
  : subst (shift u1) u2 = u1.
Proof.
  revert g1 g2 t t0 u1 u2.
  fix self 5.
  intros.
  destruct u1; cbn.
  - admit.
  - rewrite 2 self. reflexivity.
  - rewrite self. reflexivity.
Admitted.

Lemma shift_shift_comm {g1 g t t0 t1} (u : tm (g1 ++ g) t)
  : shift (g1 := t0 :: g1) (t0 := t1) (shift (g1 := nil) (t0 := t0) u)
    = shift (g1 := nil) (t0 := t0) (shift (g1 := g1) (t0 := t1) u).
Proof.
Admitted.

Lemma shift_shift_comm0 {g t t0 t1} (u : tm g t)
  : shift (g1 := [t0]) (t0 := t1) (shift (g1 := nil) (t0 := t0) u)
    = shift (g1 := nil) (t0 := t0) (shift (g1 := nil) (t0 := t1) u).
Proof.
  apply (shift_shift_comm (g1 := nil)).
Qed.

(**)

Definition id {A} : A ---> A := Lam (Var here).

Definition cat {A B C} (f : A ---> B) (g : B ---> C) : A ---> C :=
  Lam (App (shift0 g) (App (shift0 f) (Var here))).

Instance Proper_cat {A B C}
  : Proper (eq_tm ==> eq_tm ==> eq_tm) (@cat A B C).
Proof.
Admitted.

Instance Proper_Lam {g t1 t2}
  : Proper (eq_tm ==> eq_tm) (@Lam g t1 t2).
Proof.
Admitted.

Instance Proper_App {g t1 t2}
  : Proper (eq_tm ==> eq_tm ==> eq_tm) (@App g t1 t2).
Proof.
Admitted.

Lemma cat_assoc {A B C D} (f : A ---> B) (g : B ---> C) (h : C ---> D)
  : (cat (cat f g) h = cat f (cat g h))%tm.
Proof.
  unfold cat. cbn.
  rewrite !red_beta. cbn.
  rewrite !shift_shift_comm0.
  rewrite !subst_shift_cancel.
  reflexivity.
Qed.

Definition times {A B C D} (u1 : A ---> B) (u2 : C ---> D)
  : Prod A C ---> Prod B D :=
  Lam (Pair (App (shift0 u1) (Proj1 (Var here)))
            (App (shift0 u2) (Proj2 (Var here)))).

Lemma functor_times {A1 A2 A3 B1 B2 B3}
      (u12 : A1 ---> A2) (u23 : A2 ---> A3)
      (n12 : B1 ---> B2) (n23 : B2 ---> B3)
  : times (cat u12 u23) (cat n12 n23) = cat (times u12 n12) (times u23 n23).
Proof.
  unfold times, cat. cbn.
  rewrite !red_beta. cbn.
  rewrite !shift_shift_comm0.
  rewrite !subst_shift_cancel.
  rewrite red_proj1, red_proj2.
  reflexivity.
Qed.

Definition eval {A B} : Prod (A -> B)%ty A ---> B :=
  Lam (App (Proj1 (Var here)) (Proj2 (Var here))).

Definition curry {A B C} (u : Prod A B ---> C) : A ---> (B -> C)%ty :=
  Lam (Lam (App (shift0 (shift0 u)) (Pair (Var (there here)) (Var here)))).

Lemma eval_curry {A B C} (u : Prod A B ---> C)
  : cat (times (curry u) id) eval = u.
Proof.
  unfold cat, times, curry, id, eval. cbn.
  rewrite !red_beta. cbn.
  rewrite !red_proj1, !red_proj2.
  rewrite !red_beta. cbn.
  change (cons A) with (app [A]).
  rewrite !shift_shift_comm. cbn.
  change [A; Prod A B] with ([A; Prod A B] ++ []).
  rewrite !shift_shift_comm. cbn.
  rewrite shift_shift_comm0.
  change [Prod A B] with ([Prod A B] ++ []).
  rewrite shift_shift_comm. cbn.
  rewrite shift_shift_comm0.
Abort.
